﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generics
{
    class Program
    {
        static void Main(string[] args)
        {
            bool checking = true;
            do
            {
                try
                {
                    Console.WriteLine("input1: ");
                    int input1 = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("input2: ");
                    int input2 = Int32.Parse(Console.ReadLine());
                    bool equal = calculator.areEqual<int>(input1, input2);
                    //bool equal2 = calculator.areEqual<int>(1, "AB");
                    if (equal)
                    {
                        Console.WriteLine("Equal");
                    }
                    else
                    {
                        Console.WriteLine("Not equal");
                    }
                    checking = false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("One of the inputs wasn't a number.");
                    checking = true;
                }
            } while (checking == true);


            Console.ReadLine();
        }
    }

    public class calculator
    {
        public static bool areEqual<T>(T val1, T val2)
        {
            return val1.Equals(val2);
        }
    }
}
